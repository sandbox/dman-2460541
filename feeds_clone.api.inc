<?php
/**
 * @file
 * Hook declarations for feeds_clone.
 */

/**
 * Feeds default re-uses hook_clone_node_alter()
 *
 * This runs whenever a placeholder node is being copied.
 *
 * Use this hook to either update or sanitize values
 * you don't want the new copy to have.
 */
function hook_clone_node_alter(&$node, $original_node, $method) {
  $node->hosting_name = NULL;
  $node->aliases = NULL;
}
