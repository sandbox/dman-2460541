= Synopsis

With this module enabled, there will be an extra setting in the processer
form where you can select a sample node.
When that is set, the chosen node will be cloned and used as a base when
creating new items from a feed. This is handy when your content needs a number
of defaults pre-populated, and this data cannot be provided by the feed
items themselves.

= Usage

First set up your template node. This should be of the same content type
as you are going to import your feeds items into.
Set default values on it, such as author, field values or taxonomy tags.
It can remain unpublished.

Next set up your feed.
Use the "Node Clone processor" which is a replacement for the normal
Node processor.
Select your sample node as the 'template' in the Node Clone processor setting.
Set up mappings as usual. Where data is found, it will override the default.

When importing, your new items should resemble the template.

= Under the hood

The template used is a clone, produced in similar ways to how node_clone.module
does it. Complex content types (like webforms) may run into issues.
We call the clone_node alter() hook, so hopefully any additional support
that exists for node_clone should help us out here also.

= TODO

In Drupal7, Need to introduce arbitrary entity support.
