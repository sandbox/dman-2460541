<?php
/**
 * @file
 * Variant of FeedsNodeProcessor that will clone an existing node.
 *
 * We only overide the config from and the BuildNode templater.
 */


/**
 * Creates nodes from feed items.
 */
class NodeCloneProcessor extends FeedsNodeProcessor {

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    // Just append ours to the normal ones.
    $defaults = parent::configDefaults();
    $defaults['template_nid'] = NULL;
    return $defaults;
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {

    // Get the normal FeedsNodeProcessor config form and add a field to it.
    $form = parent::configForm($form_state);
    $form['content_type']['#weight'] = -2;
    // These settings are redundant when cloning.
    unset($form['input_format']);

    $sites = array();
    // Yeah, direct DB call. This is D6.
    if (!empty($this->config['content_type'])) {
      $result = db_query("SELECT nid, title FROM {node} WHERE status = 1 AND type = '%s' ", array($this->config['content_type']));
    }
    else {
      $result = db_query("SELECT nid, title FROM {node} WHERE status = 1");
    }
    // In D7 I would AJAX this. Can't be bothered with handmade AHA in D6.
    // Just reload the form please.
    $form['content_type']['#description'] = t('Please submit the form once to apply the new content type filter. (Sorry no AJAX)');

    while ($found = db_fetch_object($result)) {
      $sites[$found->nid] = $found->title . ' (' . $found->nid . ')';
    }

    $form['template_nid'] = array(
      '#type' => 'select',
      '#title' => t('Template node'),
      '#description' => t('Select the original node to clone.', array()),
      '#options' => $sites,
      '#default_value' => $this->config['template_nid'],
      '#weight' => -1,
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * Override parent::configFormValidate().
   */
  public function configFormValidate(&$values) {
  }
  /**
   * Reschedule if expiry time changes.
   */
  public function configFormSubmit(&$values) {
    parent::configFormSubmit($values);
  }

  /**
   * Creates a new node object cloned from the default and returns it.
   */
  protected function buildNode($nid, $feed_nid) {
    $populate = FALSE;

    if (empty($nid)) {
      $original_node = node_load($this->config['template_nid']);
      // @see node_clone.module.
      $node = drupal_clone($original_node);
      $node->nid = NULL;
      $node->vid = NULL;
      $node->tnid = NULL;
      $node->created = NULL;
      $node->menu = NULL;
      $node->path = NULL;
      $node->files = NULL;
      $populate = TRUE;
      // Emulate further node_clone behaviour.
      // Let other modules do special fixing up.
      $node->clone_from_original_nid = $original_node->nid;
      drupal_alter("clone_node", $node, $original_node, "prepopulate");
      // That hook should be used to do something like this.
      // $node->hosting_name = NULL;

    }
    else {
      if ($this->config['update_existing'] == FEEDS_UPDATE_EXISTING) {
        $node = node_load($nid, NULL, TRUE);
      }
      else {
        $node = db_fetch_object(db_query("SELECT created, nid, vid, status FROM {node} WHERE nid = %d", $nid));
        $populate = TRUE;
      }
    }
    if ($populate) {
      $node->changed = FEEDS_REQUEST_TIME;
      $node->feeds_node_item = new stdClass();
      $node->feeds_node_item->id = $this->id;
      $node->feeds_node_item->imported = FEEDS_REQUEST_TIME;
      $node->feeds_node_item->feed_nid = $feed_nid;
      $node->feeds_node_item->url = '';
      $node->feeds_node_item->guid = '';
    }

    static $included;
    if (!$included) {
      module_load_include('inc', 'node', 'node.pages');
      $included = TRUE;
    }
    node_object_prepare($node);

    // Populate properties that are set by node_object_prepare().
    $node->log = 'Created/updated by NodeCloneProcessor';
    if ($populate) {
      $node->uid = $this->config['author'];
    }
    return $node;
  }



}

